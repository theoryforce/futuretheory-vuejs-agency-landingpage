# Future Theory VueJS 2 Landing Page

> A Vue.js project by [TheoryForce](https://www.theoryforce.com)

[demo](https://futuretheory-vuejs-onepager.netlify.com)

Lightweight setup for one-page agency or creative landing page. Includes `sass` and slide-out menu.

![Future Theory VueJS 2 Landing Page](futuretheory1.png)

![Future Theory VueJS 2 Landing Page](futuretheory2.png)


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Styling

* Main stylesheet: `src/assets/style.scss`
* Text content: `src/assets/App.vue`

Logo in this template is textual. The brand name for the app content is defined via the Vue `app`:

```
data () {
  return {
    italic: 'future',
    normal: 'theory'
  }
```

This template is not meant for content-heavy websites.

## Note

Consider the [license](/LICENSE).
